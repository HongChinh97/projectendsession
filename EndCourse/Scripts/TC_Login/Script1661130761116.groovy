import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import javax.xml.bind.annotation.XmlElementDecl.GLOBAL

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'1. Open brower tranpora'
WebUI.openBrowser('http://210.245.85.229:9997/login')

WebUI.maximizeWindow()
'2. Input username'
WebUI.setText(findTestObject('Object Repository/Tranpora/input_withPlaceHolder', ['name': 'Email address']), 'anhmmosg3@gmail.com')

'3. Input password'
WebUI.setEncryptedText(findTestObject('Object Repository/Tranpora/input_withPlaceHolder', ['name':'Password']), 'aeHFOx8jV/A=')

'4. Click button sign in'
WebUI.click(findTestObject('Object Repository/Tranpora/btn_submit'))

'5. Verify text login success'
WebUI.verifyElementText(findTestObject('Object Repository/Tranpora/lbl_withText', ['text':'Cookie preferences']), 'Cookie preferences')

'6. Click on accept all button'
WebUI.click(findTestObject('Object Repository/Tranpora/btn_action', ['Id':'s-all-bn']))
