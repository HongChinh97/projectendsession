import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.Key

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import geeks.Main

Main main = new Main()
'1. Open brower tranpora'
WebUI.openBrowser('https://www.geeksforgeeks.org/')

WebUI.maximizeWindow()

'2. Click search button'
WebUI.click(findTestObject('Object Repository/Geeks/btn_search'))

'3. Input text search'
WebUI.setText(findTestObject('Object Repository/Geeks/input_withId', ['Id':'gcse-search-input']), 'data structer')
WebUI.sendKeys(findTestObject('Object Repository/Geeks/input_withId', ['Id':'gcse-search-input']), Keys.chord(Keys.ENTER))
 
'4. Verify data search and click element search'
main.verifyElement(findTestObject('Object Repository/Geeks/list_search'), txtExpect)
