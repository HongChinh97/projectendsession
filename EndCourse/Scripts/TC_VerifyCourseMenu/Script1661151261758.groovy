import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import geeks.Main

Main main = new Main()
'1. Call test case search geeks successfully'
WebUI.callTestCase(findTestCase('TC_Search Geeks Successfully'), [('txtExpect') : 'Data Structures - GeeksforGeeks'], FailureHandling.STOP_ON_FAILURE)

'2. Click menu'
WebUI.mouseOver(findTestObject('Object Repository/Geeks/drp_menuList', [('main'):'header-main__list-item selected']))

WebUI.click(findTestObject('Object Repository/Geeks/drp_subMenu'))

txt = WebUI.getText(findTestObject('Object Repository/Geeks/txt_problemTheDay'))
'3. Verify text '
WebUI.verifyEqual(txt, expect)