<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_dataStructor</name>
   <tag></tag>
   <elementGuidId>a697301a-96fc-475e-b4fe-639b4a00632d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@class,'courseNameAdjust') and text()='Data Structures and Algorithms - Self Paced']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
