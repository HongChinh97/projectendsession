<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_problemTheDay</name>
   <tag></tag>
   <elementGuidId>32f10a4b-00da-49d1-b16d-208d6964f78a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;scrollableDiv&quot;]//div[1]//p[contains(text(),'Immediate Smaller Element')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
