<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_search</name>
   <tag></tag>
   <elementGuidId>bc18cc37-1492-48f4-9ae7-5bba66e0f531</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[@class=&quot;gfg-icon gfg-icon_search gfg-icon_white gcse-search__icon&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
